package com.tarek.recipes.recepie;

import com.tarek.recipes.MockObjectBuilder;
import com.tarek.recipes.recipe.aggregate.Recipe;
import com.tarek.recipes.recipe.commands.CreateRecipeCommand;
import com.tarek.recipes.recipe.events.RecipeCreatedEvent;
import org.axonframework.test.aggregate.AggregateTestFixture;
import org.axonframework.test.aggregate.FixtureConfiguration;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class RecipeTest {

    private static final String recipeId = UUID.randomUUID().toString();
    private static final String name = "potato salad";
    private static final String description = "add potato make salad";
    private static final int numberOfServings = 1;

    private FixtureConfiguration<Recipe> fixture;

    @Before
    public void setUp() {
        fixture = new AggregateTestFixture<>(Recipe.class);
    }
    @Test
    public void testCreateNewUser() {
        fixture.givenState(() -> new MockObjectBuilder<Recipe>(Recipe.class)
                        .set("id", recipeId)
                        .build()
                ).when(new CreateRecipeCommand(
                        recipeId,
                        "potato salad",
                        "add potato make salad",
                        1,
                        "vegan",
                        "potatoes,salad")
                )
                .expectSuccessfulHandlerExecution()
                .expectEvents(new RecipeCreatedEvent(
                        recipeId,
                        "potato salad",
                        "add potato make salad",
                        1,
                        "vegan",
                        "potatoes,salad")
                )
                .expectState(state -> {
                    assertEquals(recipeId, state.getId());
                    assertEquals(name, state.getName());
                    assertEquals(description, state.getDescription());
                    assertEquals(numberOfServings, state.getNumberOfServings());
                    assertEquals("potatoes,salad", state.getIngredients());
                });
    }


}
