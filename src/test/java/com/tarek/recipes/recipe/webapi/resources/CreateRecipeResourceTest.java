package com.tarek.recipes.recipe.webapi.resources;
import com.tarek.recipes.recipe.commands.CreateRecipeCommand;
import org.axonframework.commandhandling.CommandBus;
import org.axonframework.commandhandling.CommandMessage;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JsonContentAssert;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.matchesPattern;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(CreateRecipeResource.class)
public class CreateRecipeResourceTest {

    @Autowired
    private MockMvc mvc;

    @Captor
    private ArgumentCaptor<CommandMessage<CreateRecipeCommand>> captor;

    @MockBean
    private CommandBus commandBus;

    @Test
    public void test_create_recipe() throws Exception {
        String name = "potato salad";
        mvc.perform(post("/api/v1/recipe")
                        .content("{ \"name\": \"potato salad\", \"description\": \"make a salad\"}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                 .andExpect(jsonPath("$.id").value( matchesPattern("[0-9a-fA-F]{8}(?:-[0-9a-fA-F]{4}){3}-[0-9a-fA-F]{12}") ))
                .andExpect(jsonPath("$.name").value(name));


        verify(commandBus, times(1)).dispatch(captor.capture());
        CommandMessage<CreateRecipeCommand> command = captor.getValue();
        Assert.assertEquals(name, command.getPayload().getName());
    }
}