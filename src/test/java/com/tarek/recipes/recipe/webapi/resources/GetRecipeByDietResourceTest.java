package com.tarek.recipes.recipe.webapi.resources;

import com.tarek.recipes.recipe.models.RecipeModel;
import org.axonframework.messaging.responsetypes.ResponseType;
import org.axonframework.queryhandling.QueryGateway;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.boot.test.mock.mockito.MockBean;
@RunWith(SpringRunner.class)
@WebMvcTest(GetRecipesByDietResource.class)
public class GetRecipeByDietResourceTest {

    private static final List<RecipeModel> RECIPES = List.of(
            new RecipeModel(UUID.randomUUID().toString(), "potato salad", "make salad",2,"vegan","potates,salad"),
            new RecipeModel(UUID.randomUUID().toString(), "potato salad", "make salad",2,"vegan","potates,salad")
    );


    @Autowired
    private MockMvc mvc;

    @MockBean
    @Qualifier("queryGateway")
    private QueryGateway queryGateway;

    @Test
    public void testGetRecipeByDiet() throws Exception {

        when(queryGateway.query(any(), any(ResponseType.class)))
                .thenReturn(CompletableFuture.completedFuture(RECIPES));

        mvc.perform(get("/api/v1/recipe/getBydiet")
                        .content("{ \"diet\": \"vegan\"}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.resources[0].id").value(RECIPES.get(0).getId()))
                .andExpect(jsonPath("$.resources[0].name").value(RECIPES.get(0).getName()))
                .andExpect(jsonPath("$.resources[0].description").value(RECIPES.get(0).getDescription()))
                .andExpect(jsonPath("$.resources[1].id").value(RECIPES.get(1).getId()))
                .andExpect(jsonPath("$.resources[1].name").value(RECIPES.get(1).getName()))
                .andExpect(jsonPath("$.resources[1].description").value(RECIPES.get(1).getDescription()))
                .andExpect(jsonPath("$.totalResults").value(RECIPES.size()))
                .andExpect(jsonPath("$.resources").isArray())
                .andExpect(jsonPath("$.resources.length()").value(RECIPES.size()));
    }

}