package com.tarek.recipes.recipe.aggregate;

import com.tarek.recipes.MockObjectBuilder;
import com.tarek.recipes.recipe.commands.CreateRecipeCommand;
import com.tarek.recipes.recipe.events.RecipeCreatedEvent;
import org.axonframework.test.aggregate.AggregateTestFixture;
import org.axonframework.test.aggregate.FixtureConfiguration;
import org.axonframework.test.aggregate.ResultValidator;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.*;

public class RecipeTest {

    private static final String recipeId = UUID.randomUUID().toString();
    private FixtureConfiguration<Recipe> fixture;
    @Before
    public void setUp() {
        fixture = new AggregateTestFixture<>(Recipe.class);
    }

    @Test
    public void test_create_new_recipe() {
        fixture.givenState(() -> new MockObjectBuilder<Recipe>(Recipe.class)
                        .set("id", recipeId)
                        .build()
                ).when(new CreateRecipeCommand(recipeId, "potato salad", "make potato make salad",1,"vegan","potato"))
                .expectSuccessfulHandlerExecution()
                .expectEvents(new RecipeCreatedEvent(recipeId, "potato salad", "make potato make salad",1,"vegan","potato"))
                .expectState(state -> {
                    assertEquals(recipeId, state.getId());
                    assertEquals("potato salad", state.getName());
                    assertEquals("make potato make salad", state.getDescription());
                });
    }


}