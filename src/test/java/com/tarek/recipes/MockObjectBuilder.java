package com.tarek.recipes;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;

public class MockObjectBuilder<T> {

    final Class<T> typeParameterClass;

    private HashMap<String, Object> attributes = new HashMap<>();

    public MockObjectBuilder(Class<T> typeParameterClass) {
        this.typeParameterClass = typeParameterClass;
    }

    public MockObjectBuilder<T> set(String key, Object value) {
        this.attributes.put(key, value);
        return this;
    }

    public T build() {
        final ObjectMapper mapper = new ObjectMapper();

        T object = mapper.convertValue(attributes, typeParameterClass);

        attributes = new HashMap<>();

        return object;
    }
}

