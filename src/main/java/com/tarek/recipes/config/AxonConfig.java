package com.tarek.recipes.config;

import com.tarek.recipes.recipe.aggregate.Recipe;
import org.axonframework.common.jpa.EntityManagerProvider;
import org.axonframework.eventhandling.EventBus;
import org.axonframework.modelling.command.GenericJpaRepository;
import org.axonframework.serialization.Serializer;
import org.axonframework.serialization.json.JacksonSerializer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.UUID;

@Configuration
public class AxonConfig {

    @Bean("eventSerializer")
    public Serializer buildSerializer() {
        return JacksonSerializer.builder()
                .lenientDeserialization()
                .build();
    }
    @Bean
    public GenericJpaRepository<Recipe> userRepository(EntityManagerProvider entityManagerProvider,
                                                       @Qualifier("eventBus") EventBus eventBus) {
        GenericJpaRepository.Builder<Recipe> builder = GenericJpaRepository.builder(Recipe.class);
        builder.identifierConverter(UUID::fromString)
                .entityManagerProvider(entityManagerProvider)
                .eventBus(eventBus);
        return builder.build();
    }

}
