package com.tarek.recipes.recipe.webapi.reponses;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@AllArgsConstructor
public class CreateRecipeResponse {

    @Getter
    private final String id;

    @Getter
    private final String name;

    @Getter
    private final String description;

    @Getter
    private final int numberOfServings;

    @Getter
    private String diet;

    @Getter
    private String ingredients;
}
