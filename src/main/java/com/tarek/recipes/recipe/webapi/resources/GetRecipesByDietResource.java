package com.tarek.recipes.recipe.webapi.resources;

import com.tarek.recipes.recipe.models.RecipeModel;
import com.tarek.recipes.recipe.queries.FetchRecipesByDietQuery;
import com.tarek.recipes.recipe.webapi.reponses.RecipesListResponse;
import com.tarek.recipes.recipe.webapi.reponses.RecipeNotFound;
import com.tarek.recipes.recipe.webapi.requests.GetRecipesByDietRequest;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.GenericQueryMessage;
import org.axonframework.queryhandling.QueryBus;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/api/v1/recipe")
public class GetRecipesByDietResource {

    // todo document me
    private final QueryGateway queryGateway;

    public GetRecipesByDietResource(@Qualifier("queryGateway") QueryGateway queryGateway) {
        this.queryGateway = queryGateway;
    }

    @GetMapping(value = "getBydiet", consumes = "application/json")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<?> createUser(@Valid @RequestBody GetRecipesByDietRequest getRecipesByDietRequest) throws ExecutionException, InterruptedException {

        FetchRecipesByDietQuery query = new FetchRecipesByDietQuery(getRecipesByDietRequest.getDiet());

        List<RecipeModel> recipes = queryGateway.query(query, ResponseTypes.multipleInstancesOf(RecipeModel.class)).get();

        if (recipes.isEmpty()) {
            return new ResponseEntity<>(new RecipeNotFound("Recipes not found for: ".concat(getRecipesByDietRequest.getDiet())), HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(new RecipesListResponse<>(recipes));
    }
}
