package com.tarek.recipes.recipe.webapi.requests;

import lombok.Getter;
import org.springframework.stereotype.Component;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.UUID;

@Component
public class CreateRecipeRequest {

    @Getter
    private String id;
    @NotBlank
    @Getter
    private String name;
    @Getter
    private String description;
    @Getter
    private int numberOfServings;

    @Getter
    private String diet = "";

    @Getter
    private String ingredients = "";

    public CreateRecipeRequest(String name, String description, int numberOfServings, String diet, String ingredients) {
        this.name = name;
        this.description = description;
        this.numberOfServings = numberOfServings;
        this.diet = diet;
        this.ingredients = ingredients;
    }

    public CreateRecipeRequest(){
        this.id = UUID.randomUUID().toString();
    }
}
