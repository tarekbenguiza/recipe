package com.tarek.recipes.recipe.webapi.resources;

import com.tarek.recipes.recipe.commands.CreateRecipeCommand;
import com.tarek.recipes.recipe.commands.UpdateRecipeCommand;
import com.tarek.recipes.recipe.webapi.reponses.CreateRecipeResponse;
import com.tarek.recipes.recipe.webapi.requests.CreateRecipeRequest;
import com.tarek.recipes.recipe.webapi.requests.UpdateRecipeRequest;
import com.tarek.recipes.validators.ValidUUID;
import org.axonframework.commandhandling.CommandBus;
import org.axonframework.commandhandling.GenericCommandMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/recipe")
public class UpdateRecipeResource {

    // todo document me
    private final CommandBus commandBus;

    public UpdateRecipeResource(CommandBus commandBus) {
        this.commandBus = commandBus;
    }

    @PutMapping(value = "/{id}", consumes = "application/json")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<?> createUser(@ValidUUID  @PathVariable UUID id ,@Valid @RequestBody UpdateRecipeRequest updateRecipeRequest) {
        UpdateRecipeCommand updateRecipeCommand = new UpdateRecipeCommand(
                id.toString(),
                updateRecipeRequest.getName(),
                updateRecipeRequest.getDescription(),
                updateRecipeRequest.getNumberOfServings(),
                updateRecipeRequest.getDiet(),
                updateRecipeRequest.getIngredients()

        );
        commandBus.dispatch(GenericCommandMessage.asCommandMessage(updateRecipeCommand));

        CreateRecipeResponse response = new CreateRecipeResponse(
                id.toString(),
                updateRecipeRequest.getName(),
                updateRecipeRequest.getDescription(),
                updateRecipeRequest.getNumberOfServings(),
                updateRecipeRequest.getDiet(),
                updateRecipeRequest.getIngredients()
        );

        return ResponseEntity.ok(response);
    }
}
