package com.tarek.recipes.recipe.webapi.resources;

import com.tarek.recipes.recipe.commands.CreateRecipeCommand;
import com.tarek.recipes.recipe.webapi.reponses.CreateRecipeResponse;
import com.tarek.recipes.recipe.webapi.requests.CreateRecipeRequest;
import org.axonframework.commandhandling.CommandBus;
import org.axonframework.commandhandling.GenericCommandMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/api/v1/recipe")
public class CreateRecipeResource {

    // todo document me
    private final CommandBus commandBus;

    public CreateRecipeResource(CommandBus commandBus) {
        this.commandBus = commandBus;
    }

    @PostMapping(value = "", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    ResponseEntity<?> createUser(@Valid @RequestBody CreateRecipeRequest createRecipeRequest) {
        CreateRecipeCommand createRecipeCommand = new CreateRecipeCommand(
                createRecipeRequest.getId(),
                createRecipeRequest.getName(),
                createRecipeRequest.getDescription(),
                createRecipeRequest.getNumberOfServings(),
                createRecipeRequest.getDiet(),
                createRecipeRequest.getIngredients()

        );
        commandBus.dispatch(GenericCommandMessage.asCommandMessage(createRecipeCommand));

        CreateRecipeResponse response = new CreateRecipeResponse(
                createRecipeRequest.getId(),
                createRecipeRequest.getName(),
                createRecipeRequest.getDescription(),
                createRecipeRequest.getNumberOfServings(),
                createRecipeRequest.getDiet(),
                createRecipeRequest.getIngredients()
        );

        URI resourceLocationUri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(createRecipeRequest.getId())
                .toUri();

        return ResponseEntity.created(resourceLocationUri).body(response);
    }
}
