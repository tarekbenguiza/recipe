package com.tarek.recipes.recipe.webapi.reponses;

import com.tarek.recipes.recipe.models.RecipeModel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
public class RecipesListResponse<T> {
    @Getter
    private int totalResults;

    @Getter
    private List<T> resources;

    public RecipesListResponse(List<T> resources) {
        this.totalResults = resources.size();
        this.resources = resources;
    }

}

