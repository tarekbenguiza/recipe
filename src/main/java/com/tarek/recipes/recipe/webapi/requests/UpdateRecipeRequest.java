package com.tarek.recipes.recipe.webapi.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import java.util.UUID;

@NoArgsConstructor
@Component
public class UpdateRecipeRequest {
    @NotBlank
    @Getter
    private String name;
    @Getter
    private String description;
    @Getter
    private int numberOfServings;

    @Getter
    private String diet ;

    @Getter
    private String ingredients;
}
