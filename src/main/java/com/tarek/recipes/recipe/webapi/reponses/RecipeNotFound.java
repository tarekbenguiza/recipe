package com.tarek.recipes.recipe.webapi.reponses;

import lombok.Getter;

public class RecipeNotFound {
    @Getter
    private String detail;

    @Getter
    private int status = 404;


    public RecipeNotFound(String detail) {
        this.detail = detail;
    }
}
