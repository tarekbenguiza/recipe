package com.tarek.recipes.recipe.webapi.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;

@NoArgsConstructor
@Component
public class GetRecipesByDietRequest {

    @NotBlank
    @Getter
    private String diet ;

}
