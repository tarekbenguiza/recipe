package com.tarek.recipes.recipe.repositories;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import java.io.Serializable;

@NoRepositoryBean
public interface ReadOnlyRepository<TYPE, ID extends Serializable>  extends Repository<TYPE, ID> {


}
