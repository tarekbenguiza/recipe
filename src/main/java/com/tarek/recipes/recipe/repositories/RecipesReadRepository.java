package com.tarek.recipes.recipe.repositories;

import com.tarek.recipes.recipe.aggregate.Recipe;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@org.springframework.stereotype.Repository
public interface RecipesReadRepository extends Repository<Recipe, String> {
    List<Recipe> findAll();

    @Query(value = "SELECT r.* FROM recipe as r WHERE r.diet = ?1", nativeQuery = true)
    Optional<Recipe> findRecipeByDiet(String diet);

}
