package com.tarek.recipes.recipe.queries.handlers;

import com.tarek.recipes.recipe.models.RecipeModel;
import com.tarek.recipes.recipe.queries.FetchRecipesByDietQuery;
import com.tarek.recipes.recipe.repositories.RecipesReadRepository;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class FetchRecipesByDietQueryHandler {


    private final RecipesReadRepository repository;

    public FetchRecipesByDietQueryHandler(RecipesReadRepository repository) {
        this.repository = repository;
    }

    @QueryHandler
    public Optional<RecipeModel> handle(FetchRecipesByDietQuery fetchRecipesByDietQuery){
        return repository.findRecipeByDiet(fetchRecipesByDietQuery.getDiet())
                .map(r -> new RecipeModel(r.getId(), r.getName(), r.getDescription(), r.getNumberOfServings(), r.getDiet(),r.getIngredients()));
    }
}
