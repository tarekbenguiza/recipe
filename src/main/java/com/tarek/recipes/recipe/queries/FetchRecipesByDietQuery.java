package com.tarek.recipes.recipe.queries;

import lombok.Getter;

public class FetchRecipesByDietQuery {

    @Getter
    private final String diet;

    public FetchRecipesByDietQuery(String diet) {
        this.diet = diet;
    }
}