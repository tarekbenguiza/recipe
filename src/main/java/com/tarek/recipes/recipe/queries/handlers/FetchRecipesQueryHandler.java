package com.tarek.recipes.recipe.queries.handlers;

import com.tarek.recipes.recipe.models.RecipeModel;
import com.tarek.recipes.recipe.queries.FetchRecipesQuery;
import com.tarek.recipes.recipe.repositories.RecipesReadRepository;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class FetchRecipesQueryHandler {

    private final RecipesReadRepository repository;

    public FetchRecipesQueryHandler(RecipesReadRepository repository) {
        this.repository = repository;
    }

    @QueryHandler
    public List<RecipeModel> handle(FetchRecipesQuery fetchRecipesQuery) {
        return repository.findAll()
                .stream()
                .map(r -> new RecipeModel(
                        r.getId(),
                        r.getName(),
                        r.getDescription(),
                        r.getNumberOfServings(),
                        r.getDiet(),
                        r.getIngredients()
                ))
                .collect(Collectors.toList());
    }

}
