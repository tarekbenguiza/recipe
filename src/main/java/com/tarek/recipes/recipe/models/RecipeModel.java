package com.tarek.recipes.recipe.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Id;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RecipeModel {

    @Id
    @Getter
    String id;
    @Getter
    @JsonProperty("name")
    String name;

    @Getter
    @JsonProperty("description")
    String description;

    @Getter
    @JsonProperty("numberOfServings")
    int numberOfServings;

    @Getter
    @JsonProperty("diet")
    private String diet = "";

    @Getter
    @JsonProperty("ingredient")
    private String ingredients = "";

}
