package com.tarek.recipes.recipe.aggregate;

import com.tarek.recipes.recipe.commands.CreateRecipeCommand;
import com.tarek.recipes.recipe.commands.UpdateRecipeCommand;
import com.tarek.recipes.recipe.events.RecipeCreatedEvent;
import com.tarek.recipes.recipe.events.RecipeUpdatedEvent;
import lombok.Getter;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.modelling.command.AggregateCreationPolicy;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.CreationPolicy;
import org.axonframework.spring.stereotype.Aggregate;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;
import java.util.UUID;
import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@Entity(name = "recipe")
@Aggregate
public class Recipe {

    @Id
    @AggregateIdentifier
    @Column(name = "id", length = 256, unique = true, nullable = false)
//    @Type(type="uuid-char")
    @Getter
    private String id;

    @Getter
    private String name = "";
    @Getter
    private String description = "";

    @Getter
    private int numberOfServings = 1;

    @Getter
    private String diet = "";

    @Getter
    private String ingredients = "";
    // TODO refactor me to  new aggregate

    @CommandHandler
    @CreationPolicy(AggregateCreationPolicy.CREATE_IF_MISSING)
    public void handle(CreateRecipeCommand cmd) {
        id = String.valueOf(cmd.getRecipeId());
        name = cmd.getName();
        description = cmd.getDescription();
        numberOfServings = cmd.getNumberOfServings();
        diet = cmd.getDiet();
        ingredients = cmd.getIngredients();
        apply(new RecipeCreatedEvent(id,name,description,numberOfServings,diet,ingredients));
    }

    @CommandHandler
    @CreationPolicy(AggregateCreationPolicy.CREATE_IF_MISSING)
    public void handle(UpdateRecipeCommand cmd) {
        id = String.valueOf(cmd.getRecipeId());
        name = cmd.getName();
        description = cmd.getDescription();
        numberOfServings = cmd.getNumberOfServings();
        diet = cmd.getDiet();
        ingredients = cmd.getIngredients();
        apply(new RecipeUpdatedEvent(id,name,description,numberOfServings,diet,ingredients));
    }

    public Recipe(){}

}