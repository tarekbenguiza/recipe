package com.tarek.recipes.recipe.commands;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@AllArgsConstructor
public class UpdateRecipeCommand {

    //TODO: improve me ValueObject
    @TargetAggregateIdentifier
    @Getter
    private final String recipeId;

    @Getter
    private final String name;
    @Getter
    private final String description;

    @Getter
    private final int numberOfServings;

    @Getter
    private String diet;

    @Getter
    private String ingredients;

}
