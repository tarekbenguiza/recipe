package com.tarek.recipes.recipe.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
public class RecipeCreatedEvent {

    @Getter
    @TargetAggregateIdentifier
    private String recipeId;

    @Getter
    private String name;

    @Getter
    private String description;

    @Getter
    private int numberOfServings;

    @Getter
    private String diet;

    @Getter
    private String ingredients;
}
